package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysOss;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.SysOssRequest;
import java.util.List;

/**
 * 文件存储接口
 *
 * @author mars
 * @date 2023-11-20
 */
public interface ISysOssService {
    /**
     * 新增
     *
     * @param param param
     * @return SysOss
     */
    SysOss add(SysOssRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysOssRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysOss
     */
    SysOss getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysOss>
     */
    PageInfo<SysOss> pageList(SysOssRequest param);

}
