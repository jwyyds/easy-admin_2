package com.mars.module.tool.controller;


import java.util.Arrays;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.tool.service.ISysOperLogService;
import com.mars.module.tool.request.SysOperLogRequest;

/**
 * 操作日志记录控制层
 *
 * @author mars
 * @date 2023-11-17
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "操作日志记录接口管理",tags = "操作日志记录接口管理")
@RequestMapping("/admin/sysOperLog" )
public class SysOperLogController {

    private final ISysOperLogService iSysOperLogService;

    /**
     * 分页查询操作日志记录列表
     */
    @ApiOperation(value = "分页查询操作日志记录列表")
    @PostMapping("/pageList")
    public R list(@RequestBody SysOperLogRequest sysOperLog) {
        return R.success(iSysOperLogService.pageList(sysOperLog));
    }

    /**
     * 获取操作日志记录详细信息
     */
    @ApiOperation(value = "获取操作日志记录详细信息")
    @GetMapping(value = "/query/{operId}" )
    public R getInfo(@PathVariable("operId" ) Long operId) {
        return R.success(iSysOperLogService.getById(operId));
    }

    /**
     * 新增操作日志记录
     */
    @ApiOperation(value = "新增操作日志记录")
    @PostMapping("/add")
    public R add(@RequestBody SysOperLogRequest sysOperLog) {
        iSysOperLogService.add(sysOperLog);
        return R.success();
    }

    /**
     * 修改操作日志记录
     */
    @ApiOperation(value = "修改操作日志记录")
    @PostMapping("/update")
    public R edit(@RequestBody SysOperLogRequest sysOperLog) {
        iSysOperLogService.update(sysOperLog);
        return R.success();
    }

    /**
     * 删除操作日志记录
     */
    @ApiOperation(value = "删除操作日志记录")
    @PostMapping("/delete/{operIds}" )
    public R remove(@PathVariable Long[] operIds) {
        iSysOperLogService.deleteBatch(Arrays.asList(operIds));
        return R.success();
    }
}
