package com.mars.module.tool.service;


import com.mars.common.request.tool.GenCodeDeployRequest;
import com.mars.common.request.tool.GenCodeRemoveRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.response.tool.TableListResponse;
import com.mars.module.tool.entity.GenTable;
import com.mars.module.tool.request.CreateTableRequest;
import com.mars.module.tool.request.GenTableImportRequest;
import com.mars.module.tool.request.GenTableRequest;
import com.mars.module.tool.response.GenTableResponse;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * 业务 服务层
 *
 * @author mars
 */
public interface IGenTableService {
    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    List<GenTable> selectGenTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    List<TableListResponse> selectDbTableListByNames(List<String> tableNames);

    /**
     * 查询业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
    GenTable selectGenTableById(Long id);

    /**
     * 修改业务
     *
     * @param genTable 业务信息
     */
    void updateGenTable(GenTable genTable);

    /**
     * 删除业务信息
     *
     * @param tableIds 需要删除的表数据ID
     */
    void deleteGenTableByIds(Long[] tableIds);

    /**
     * 导入表结构
     *
     * @param request 导入表列表
     */
    void importGenTable(GenTableImportRequest request);

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    Map<String, String> previewCode(Long tableId);

    /**
     * 生成代码
     *
     * @param tableName 表名称
     * @return 数据
     */
    byte[] generatorCode(String tableName);

    /**
     * 批量生成代码
     *
     * @param tableNames 表数组
     * @return 数据
     */
    byte[] generatorCode(String[] tableNames);

    /**
     * 修改保存参数校验
     *
     * @param genTable 业务信息
     */
    void validateEdit(GenTable genTable);

    /**
     * 一键部署代码
     *
     * @param request request
     */
    void deploy(GenCodeDeployRequest request);

    /**
     * 卸载
     *
     * @param request request
     */
    void remove(GenCodeRemoveRequest request);

    /**
     * 保存 菜单
     *
     * @param moduleName   moduleName
     * @param functionName functionName
     * @param businessName businessName
     * @param request      request
     * @param tableName    tableName
     */
    void saveSysMenu(String moduleName, String functionName, String businessName, GenCodeDeployRequest request, String tableName);


    /**
     * 获取已导入表结构
     *
     * @param request 请求参数
     * @return List<GenTableResponse>
     */
    PageInfo<GenTableResponse> genTableList(GenTableRequest request);

    /**
     * 删除已导入表
     *
     * @param id id
     */
    void deleteGenTableById(Long id);

    /**
     * 查询已导入表详情
     *
     * @param id id
     * @return GenTableResponse
     */
    GenTableResponse detail(Long id);

    /**
     * 一键建表
     *
     * @param request request
     */
    void createTable(CreateTableRequest request) throws SQLException;
}
