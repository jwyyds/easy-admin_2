package com.mars.module.tool.service;

import com.mars.module.tool.entity.SysOperLog;
import com.mars.common.response.PageInfo;
import com.mars.module.tool.request.SysOperLogRequest;
import java.util.List;

/**
 * 操作日志记录接口
 *
 * @author mars
 * @date 2023-11-17
 */
public interface ISysOperLogService {
    /**
     * 新增
     *
     * @param param param
     * @return SysOperLog
     */
    SysOperLog add(SysOperLogRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long operId);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> operIds);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysOperLogRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysOperLog
     */
    SysOperLog getById(Long operId);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysOperLog>
     */
    PageInfo<SysOperLog> pageList(SysOperLogRequest param);

}
