package com.mars.module.system.controller;

import com.mars.common.enums.BusinessType;
import com.mars.common.request.sys.SysPostRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.framework.annotation.Log;
import com.mars.module.system.entity.SysPost;
import com.mars.module.system.service.ISysPostService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-03 23:05:39
 */
@RestController
@RequestMapping("/sys/post")
@AllArgsConstructor
public class SysPostController {

    private final ISysPostService sysPostService;


    /**
     * 添加岗位
     *
     * @param request request
     */
    @Log(title = "添加岗位", businessType = BusinessType.INSERT)
    @ApiOperation(value = "添加岗位")
    @PostMapping("/add")
    public R add(@Validated @RequestBody SysPostRequest request) {
        sysPostService.add(request);
        return R.success();
    }

    /**
     * 更新岗位信息
     *
     * @param request request
     */
    @Log(title = "更新岗位信息", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "更新岗位信息")
    @PostMapping("/update")
    public R update(@Validated @RequestBody SysPostRequest request) {
        sysPostService.update(request);
        return R.success();
    }


    /**
     * 删除岗位
     *
     * @param id id
     */
    @Log(title = "更新岗位信息", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除岗位")
    @PostMapping("/delete/{id}")
    public R delete(@PathVariable("id") Long id) {
        sysPostService.delete(id);
        return R.success();
    }

    /**
     * 分页查询
     *
     * @param request 请求参数
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("/pageList")
    public R pageList(@RequestBody SysPostRequest request) {
        return R.success(sysPostService.pageList(request));
    }

    /**
     * 岗位列表
     *
     * @return List<SysPost>
     */
    @ApiOperation(value = "岗位列表")
    @GetMapping("/list")
    public R getList() {
        return R.success(sysPostService.getList());
    }

    /**
     * 获取详情
     *
     * @param id id
     * @return R
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取详情")
    public R get(@PathVariable("id") Long id) {
        return R.success(sysPostService.detail(id));
    }

}
