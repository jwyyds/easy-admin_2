package com.mars.common.response.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysFileResponse {

    @ApiModelProperty(value = "文件ID")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String fileName;

    @ApiModelProperty(value = "后缀")
    private String suffix;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "大小（B）")
    private Long fileSize;

    @ApiModelProperty(value = "内容类型")
    private String contentType;


}
